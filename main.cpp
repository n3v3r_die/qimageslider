#include "qimageslider.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QImageSlider w;
    w.resize(QSize(500, 500));
    for (int i = 0; i < 4; i++){
        w.addImage(QString("://splash%1.png").arg(i));
    }
    w.setRandomImage();
    w.show();
    return a.exec();
}
