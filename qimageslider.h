#ifndef QIMAGESLIDER_H
#define QIMAGESLIDER_H

#include <QWidget>

class QLabel;
class QHBoxLayout;

class QImageSlider : public QWidget
{
    Q_OBJECT
public:
    explicit QImageSlider(QWidget *parent = nullptr);
    void addImage(const QString &img);
    void setRandomImage();

private slots:
    void on_pushButton_clicked();

private:
    QLabel* m_label;
    QVector <QPixmap> m_images;
    QHBoxLayout* m_buttonsLayout;
    void setActiveButton(int index);
    void setActiveImage(int imageIndex);
protected:
    virtual void resizeEvent(QResizeEvent *event) override;
};

#endif // QIMAGESLIDER_H
