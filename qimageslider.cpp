#include "qimageslider.h"
#include "ui_widget.h"
#include <QStyle>
#include <QLabel>
#include <QBoxLayout>
#include <QPushButton>
#include <QResizeEvent>

QImageSlider::QImageSlider(QWidget *parent)
    : QWidget(parent)
    , m_label(new QLabel(this))
    , m_buttonsLayout(new QHBoxLayout)
{
    auto mainLayout = new QVBoxLayout(this);
    auto bottomLayout = new QHBoxLayout;
    mainLayout->addStretch();
    mainLayout->addLayout(bottomLayout);
    bottomLayout->addStretch();
    bottomLayout->addLayout(m_buttonsLayout);
    bottomLayout->addStretch();
    this->setStyleSheet("QPushButton { border: 0px; background-color: #ffffff; }"
                        "QPushButton:hover { background-color: #0085c8; }"
                        "QPushButton[IsActive=\"true\"] { background-color: yellow; }");
}

void QImageSlider::addImage(const QString &img)
{
    QPixmap pix(img);
    auto btn = new QPushButton(this);
    int imagesCount = m_images.length();
    btn->setCheckable(false);
    btn->setMinimumSize(QSize(12, 12));
    btn->setMaximumSize(QSize(12, 12));
    btn->setCursor(Qt::PointingHandCursor);
    btn->setProperty("imageIndex", imagesCount);
    btn->setObjectName(QString("pbSlider%1").arg(imagesCount));
    btn->setProperty("mandatoryField", true);
    m_images.push_back(pix);
    m_buttonsLayout->addWidget(btn);
    connect(btn, SIGNAL(clicked()), SLOT(on_pushButton_clicked()));
}

void QImageSlider::setRandomImage()
{
    int imageIndex = rand()%m_images.length();
    setActiveImage(imageIndex);
}

void QImageSlider::on_pushButton_clicked()
{
    bool ok;
    int imageNumber = sender()->property("imageIndex").toInt(&ok);
    if (!ok) return;
    setActiveImage(imageNumber);
}

void QImageSlider::setActiveButton(int index)
{
    QPushButton * btn;
    for (int i = 0; i < m_buttonsLayout->count(); i++) {
        btn = qobject_cast<QPushButton*>(m_buttonsLayout->itemAt(i)->widget());
        if (btn){
            if (btn->property("imageIndex") != index)
                btn->setProperty("IsActive", false);
            else
                btn->setProperty("IsActive", true);
            style()->polish(btn);
        }
    }
}

void QImageSlider::setActiveImage(int imageIndex)
{
    m_label->setPixmap(m_images[imageIndex]);
    setActiveButton(imageIndex);
}

void QImageSlider::resizeEvent(QResizeEvent *event)
{
    m_label->resize(event->size());
}
